create table wp_statut(
	Id_statut		SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Libelle_statut	Varchar(50)
); 

create table wp_axe(
	Id_axe			SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Libelle_axe		Varchar(50)
);

create table wp_equipe(
	Id_equipe		SMALLINT AUTO_INCREMENT,
	Libelle_equipe	Varchar(50),
	Id_axe			SMALLINT,
	constraint PKequipe primary key (Id_equipe,Id_axe),
	constraint FKAxe foreign key (Id_axe) references wp_axe(Id_axe) on delete cascade
);

create table wp_personnel(
	Id_pers 		SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Nom		 		Varchar(50) NOT NULL,
	Prenom			Varchar(50) NOT NULL,
	Date_naissance	DATE,
	Email1			Varchar(50),
	Email2			Varchar(50),
	Date_entrer		DATE,
	Date_sortie		DATE,
	Id_statut		SMALLINT,
	Id_equipe		SMALLINT,
	Université		Varchar(50),
	CNRS			Varchar(50),
	IUF				Varchar(50), 
	constraint FKStatut foreign key (Id_Statut) references wp_statut(Id_Statut) on delete cascade,
	constraint FKEquipe foreign key (Id_equipe) references wp_equipe(Id_equipe) on delete cascade
);

create table wp_pedr(
	Id_pedr		SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Id_pers		SMALLINT,
	Date_pedr	YEAR,
	Montant		SMALLINT,
	constraint FKPers_pedr foreign key (Id_pers) references wp_personnel(Id_pers) on delete cascade
);

create table wp_grade(
	Id_Grade		SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Id_pers			SMALLINT,
	Date_debut		DATE,
	Date_fin		DATE,
	constraint FKPers  foreign key (Id_pers) references wp_personnel(Id_pers) on delete cascade
);

create table wp_hdr(
	Id_pers			SMALLINT,
	Id_HDR			SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Date_hdr		DATE,
	Libelle_hdr		Varchar(50),
	constraint FKPers_hdr 	foreign key (Id_pers) 		references wp_personnel(Id_pers) on delete cascade
);

create table wp_these(
	Id_pers			SMALLINT,
	Id_these		SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Date_these 		DATE,
	Libelle_these	Varchar(50),
	Id_Referent		SMALLINT,
	constraint FKPers_these foreign key (Id_pers) 		references wp_personnel(Id_pers) on delete cascade,
	constraint FKDirecteur 	foreign key (Id_Referent) 	references wp_personnel(Id_pers) on delete cascade
);

create table wp_type(
	Id_type			SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Libelle_type	Varchar(50),
	Categorie		Varchar(50)
);

create table wp_documents(
	-- Commun --
	Id_doc			SMALLINT AUTO_INCREMENT PRIMARY KEY,
	Id_type			SMALLINT,
	Date_doc		DATE,
	Titre 			Varchar(50),
	Nb_page			SMALLINT,
	Numero			Varchar(50),
	Lieu_edition	Varchar(50),
	Editeur_com		Varchar(50),
	Titre_Collection Varchar(50),
	URL				Varchar(50),
	ISBN 			Varchar(50),
	Resume			Varchar(1000),
	Pays			Varchar(50),
	Ville			Varchar(50),
	Lieu_Creation	Varchar(50),
	-- Thèse --
	Domaine			Varchar(50),
	Universite		Varchar(50),
	Lieu 			Varchar(50),
	Langue			Varchar(50),
	Titre_abreger	Varchar(50),
	Jury			Varchar(500),
	Archive			Varchar(50),
	Location_archive Varchar(50),
	Catalogue		Varchar(50),
	Cote			Varchar(50),
	-- Revue --
	Titre_article	Varchar(50),
	Volume			Varchar(50),
	Pagination		Varchar(50),
	-- Communication --
	Nom_maitre		Varchar(50),
	Prenom_maitre 	Varchar(50),
	Titre_conference Varchar(50),
	Date_conf		DATE,
	Lieu_conf		Varchar(50),
	-- Ouvrage --
	Edition 		Varchar(50),
	Format			Varchar(50),
	Date_consultation DATE,
	-- Autre --
	Autre_type 		Varchar(50),
	-- Brevet --
	type_brevet		Varchar(50),
	-- Logiciel --
	Nom_logiciel	Varchar(50),
	Support_logiciel Varchar(50),
	Version 		Varchar(50),
	OS				Varchar(50),
	-- BDD --
	Commanditaire	Varchar(50),
	-- CONTRAT ---
	Date_fin 		DATE,
	Parternaire		Varchar(500),
	constraint FKtype_doc foreign key (Id_type) references wp_type(Id_type) on delete cascade
);

create table wp_auteur(
	Id_pers			SMALLINT,
	Id_doc			SMALLINT,
	constraint PKauteur primary key (Id_pers,Id_doc),
	constraint FKPers_auteur foreign key (Id_pers) references wp_personnel(Id_pers) on delete cascade,
	constraint FKDoc_auteur foreign key (Id_doc) references wp_documents(Id_doc) on delete cascade
);

