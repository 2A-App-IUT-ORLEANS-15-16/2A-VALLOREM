<?php 

//require_once('ActionTest.php');

/**
  * La classe gère le menu de personnalisation du chercheur
  *
  * @author 2A-Apprenti
  */
class ChercheurSubMenu
{
	
	function __construct()
	{
		//var_dump(add_submenu_page);
		add_action("admin_menu", [$this, 'adminMenu']); //Appel du adminMenu()
		//new ActionTest();
	}

	function adminMenu(){
		add_submenu_page(VALLOREM_MENU_SLUG, 'Mon profil chercheur', 'Mon profil chercheur', 'edit_self_profil', 'vallorem-my-chercheur', array($this, 'render')); //Creation d'un sous-menu
	}

	function render(){	
		
		include('templates/chercheur-profil.php'); 
	}	
}


 ?>