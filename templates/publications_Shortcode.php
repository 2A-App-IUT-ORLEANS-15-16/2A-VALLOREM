<?php
/**
* @author 2A-Apprenti
* @method jointure(String[] $tab,String $glue,String $prefix='',String $suffixe='')
* @method affichageParNom(Object[] $publications)
* @method affichageParDate(Object[] $publications)
* @method annee(String $publication)
* @method auteur(String $publication)
* @method these(String $publication)
* @method hdr(String $publication)
* @method communication(String $publication)
* @method article(String $publication)
* @method ouvrage(String $publication)
* @method participation(String $publication)
* @method contrat_industriel(String $publication)
* @method database(String $publication)
* @method logiciel(String $publication)
* @method autre(String $publication)
* @method brevet(String $publication)
* Cette classe permet l'affichage des différents types de documents suivant la norme ZOTERO
*/
class Affichage {

	/**
	* @var String $CurrentNom : le nom de l'utilisateur
	*/
	private $CurrentNom = "" ;
	/**
	* @var String $CurrentPrenom : le prénom de l'utilisateur
	*/
	private $CurrentPrenom = "" ;
	/**
	* @var String $CurrentDate : la date du jour
	*/
	private $CurrentDate = "";

	/**
	* Permet de joindre une liste de String avec un charactère $glue, il est possible d' ajouter un préfixe et un suffixe
	*
	* @param String[] $tab un tableau contenant l'ensemble des éléments que l'on veut joindre
	* @param String $glue le charactère qui va joindre les éléments du tableau $tab
	* @param String $prefix String qui va aller en préfixe
	* @param String $suffixe String qui va aller en suffixe
	* @return String $strtab la chaine de charactère contenant tous les éléments joints
	*/
	function jointure($tab, $glue, $prefix='', $suffixe=''){
		$strtab = implode($glue, array_filter($tab));
		if(strlen($strtab)>0){
			$strtab = $prefix.$strtab.$suffixe;
		}
		return $strtab;
	}

	/**
	* Permet l'affichage des publications ordonnées par Nom d'auteur avec un affichage différent selon le type de publication
	* @param Object[] $publications la liste de toutes les publications
	*/
	function affichageParNom($publications){
		foreach ($publications as $publication){
			$categorie = strval($publication->Categorie);
			$this->$categorie($publication);
		}
	}

	/**
	* Permet l'affichage des publications ordonnées par dte de publication avec un affichage différent selon le type de publication
	* @param Object[] $publications la liste de toutes les publications
	*/
	function affichageParDate($publications){
		foreach ($publications as $publication){
			$this->annee($publication);
			$categorie = strval($publication->Categorie);
			$this->$categorie($publication);
		}
	}

	/**
	* Affichage de la date de la publication
	* @param String $publication la publication pour laquelle on veut afficher la date
	*/
	function annee($publication){
		if (date_parse($publication->Date_doc)['year'] != $this->CurrentDate){
			$this->CurrentDate = date_parse($publication->Date_doc)['year'] ;
			echo  "<h1>".$this->CurrentDate."</h1>" ;
		}
	}

	/**
	* Affichage de l'auteur de la publication avec son nom et son prénom
	* @param String $publication la publication pour laquelle on affiche l'auteur
	*/
	function auteur($publication){
		if ($publication->Nom != $this->CurrentNom && $publication->Prenom != $this->CurrentPrenom){
			echo  "<h2>".($publication->Nom ." ". $publication->Prenom)."</h2>" ;
			$this->CurrentNom = $publication->Nom ;
			$this->CurrentPrenom = $publication->Prenom ;
		}
	}

	/**
	* Affichage du type Thèse
	* @param String $publication la publication de type thèse à afficher
	*/
	function these($publication){
			$this->auteur($publication);
			$NomPrenom = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ",'','.');
			$date = $this->jointure([$publication->date_doc],"","(",").");
			$lieu = $this->jointure([$publication->Universite,$publication->Lieu]," , "," ( "," ) ");
			$all = $this->jointure([$publication->TitreRésumé, $publication->Domaine, $publication->Nb_page, $publication->Langue, $publication->Titre_abreger, $publication->URL, $publication->Jury, $publication->Archive, $publication->Location_archive, $publication->Catalogue, $publication->Cote]," , ");

			echo "<p>".$NomPrenom.$date.$all.$lieu."</p>" ;
	}

  /**
	* Affichage du type HDR
	* @param Sring $publication la publication de type HDR à afficher
	*/
	function hdr($publication){
		$this->these($publication);
	}

	/**
	* Affichage de type communication
	* @param String $publication la publication de type communication à afficher
	*/
	function communication($publication){
			$this->auteur($publication);
			$auteur = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ");
			$titre = $publication->Titre;
			$maitre = $this->jointure([$publication->Nom_Maitre, $publication->Prenom_maitre],', ','In :');
			$conference = $this->jointure([$publication->Titre_conference, $publication->Date_conf, $publication->Lieu_conf],', ');
			$edition = $this->jointure([$publication->Lieu_edition, $publication->Editeur_com],' : ');
			$edition = $this->jointure([$edition, $publication->Date_doc, $publication->Nb_page],', ');
			$collection = $this->jointure([$publication->Titre_Collection, $publication->Numero],', ','(',')');
			$isbn = $publication->ISBN;
			echo "<p>".$this->jointure([$auteur, $titre, $maitre, $conference, $edition, $collection, $isbn],'. ','','.')."</p>";
	}

	/**
	* Affichage du type Article
	* @param String $publication la publication de type article à afficher
	*/
	function article($publication){
			$this->auteur($publication);
			$auteur = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ");
			$titre_article = $publication->Titre;
			$periodique = $this->jointure([$publication->Titre_article, $publication->Date_doc, $publication->Volume, $publication->Numero, $publication->Pagination],', ');
			$isbn = $publication->ISBN;
			echo "<p>".$this->jointure([$auteur, $titre_article, $periodique, $isbn],". ",'','.')."</p>";
	}

	/**
	* Affichage du type Ouvrage
	* @param String $publication la publication de type ouvrage à afficher
	*/
	function ouvrage($publication){
			$this->auteur($publication);
			$auteur = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ");
			$titre = $publication->Titre;
			$tomaison = $this->jointure([$publication->Volume],'','','[en ligne]');
			$edition = $publication->Edition;
			$lieu_edition = $this->jointure([$publication->Lieu_edition, $publication->Editeur_com],' : ');
			$collection = $this->jointure([$publication->Titre_Collection, $publication->Numero],', ','(',')');
			$lieu_edition = $this->jointure([$lieu_edition, $publication->Date_doc, $publication->Nb_page, $collection], ', ');
			$format = $publication->Format;
			$url = $this->jointure([$publication->URL],' : ','Disponible sur');
			$isbn = $this->jointure([$publication->ISBN],'','(',')');

			echo "<p>".$this->jointure([$auteur, $titre, $tomaison, $edition, $lieu_edition, $format, $url],'. ','','.')."</p>";
	}

	/**
	* Affichage du type Participation
	* @param String $publication la publication de type participation à afficher
	*/
	function participation($publication){
			$this->auteur($publication);
			$auteur = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ");
			$res = $this->jointure([$auteur, $publication->Titre, $publication->Date_doc, $publication->Lieu_Creation, $publication->Resume, $publication->Commanditaire],', ','','.');

			echo "<p>".$res."</p>";
	}

	/**
	* Affichage du type Contrat_industriel
	* @param String $publication la publication de type contrat_industriel à afficher
	*/
	function contrat_industriel($publication){
			$zone = $publication->Resume;
			$titre = $publication->Titre;
			$partenaires = $publication->Parternaire;
			$date = $this->jointure([ $publication->Date_doc, $publication->Date_fin],', ','Date (',')');
			$referent = $this->jointure([$publication->Nom_maitre,strtoupper(substr($publication->Prenom_maitre,0,1))],", ");

			echo "<p>".$this->jointure([$zone, $titre, $partenaires, $date, $referent],',','','.')."</p>";
	}

	/**
	* Affichage du type Base de donnée
	* @param String $publication la publication de type base de données à afficher
	*/
	function database($publication){
			$this->auteur($publication);
			$NomPrenom = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ",'','.');
			$date = $this->jointure([$publication->date_doc],"","(",").");
			$title = $this->jointure([$publication->Titre,$date],".","","[en ligne]");
			$qui = $this->jointure([$publication->Commanditaire,$publication->Ville],".",".");
			$ou = $this->jointure([$publication->Lieu_Creation],".",":");
			$url = $this->jointure([$ou,$publication->URL],".");
			echo "<p>".$NomPrenom.$title.$qui.$url."</p>";
	}

	/**
	* Affichage du type Logiciel
	* @param String $publication la publication de type logiciel à afficher
	*/
	function logiciel($publication){
			$this->auteur($publication);
			$NomPrenom = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ",'');
			$title = $this->jointure([$NomPrenom,$publication->Titre],".");
			$support = $this->jointure([$publication->Support_logiciel],"","[","]");
			$titleSupport = $this->jointure([$title,$support],"","",".");
			$versionVille = $this->jointure([$publication->Version,$publication->Ville],".","",":");
			$anneeLieu = $this->jointure([$publication->Lieu_Creation],"","",",");
			$anneeOsUrl = $this->jointure([$publication->Date_doc,$publication->OS,$publication->URL],".");

			echo "<p>".$titleSupport.$versionVille.$anneeLieu.$anneeOsUrl. "</p>";
	}

	/**
	* Affichage de type Autre
	* @param String $publication la publication de type Autre à afficher
	*/
	function autre($publication){
			$this->auteur($publication);
			$auteur = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ");
			$titre_article = $publication->Titre;
			$periodique = $this->jointure([$publication->Titre_article, $publication->Date_doc, $publication->Volume, $publication->Numero, $publication->Pagination],', ');
			$isbn = $publication->ISBN;
			$type = $publication->Autre_type;
			$res = $this->jointure([$auteur, $titre_article, $periodique, $isbn],". ",'','.');

			echo "<p>".$this->jointure([$type, $res],' : ')."</p>";
	}

	/**
	* Affichage de type Brevet
	* @param String $publication la publication de type brevet à afficher
	*/
	function brevet($publication){
			$this->auteur($publication);
			$NomPrenom = $this->jointure([$publication->Nom,strtoupper(substr($publication->Prenom,0,1))],", ",'','.');
			$title = $this->jointure([$publication->Titre,$publication->Numero],".","","[en ligne]");
			$url = $this->jointure([$publication->URL],"","Disponible sur :","");
			$joinFin = $this->jointure([$title,$publication->Date_doc,$url],".");

			echo "<p>".$NomPrenom.$joinFin. "</p>";
	}
}
?>
