<style>
	[data-id] .content{
		display: none;
		padding: 25px;
	}

	[data-id].selected .content{
		display: block;
	}
</style>

<?php foreach ($chercheurs as $chercheur): ?>
	<div data-id="<?php echo $chercheur->Id_pers ?>">
		<h4>
			<a data-nom href="#"><?php echo $chercheur->Prenom . ' ' . $chercheur->Nom ?></a>
		</h4>
		<div class="content">
			<?php if ($chercheur->Email1): ?>
				<span><b>Email : </b><a href=""><?php echo $chercheur->Email1 ?></a></span>
			<?php endif ?>
		</div>
	</div>
<?php endforeach ?>


<script>
	var as = document.querySelectorAll('[data-id] a[data-nom]');

	for(var i in Object.keys(as)){
		var a = as[i];
		console.log(a, i);
		a.addEventListener('click', function(e){
			e.preventDefault();
			if(this.parentNode.parentNode.className != "selected")
				this.parentNode.parentNode.className = "selected";
			else
				this.parentNode.parentNode.className = ""
			return false;
		});
	}
</script>
