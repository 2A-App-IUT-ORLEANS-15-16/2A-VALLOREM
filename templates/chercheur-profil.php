<div class="wrap">
	<h1>
		<?php echo get_admin_page_title() ?>
	</h1>
	<?php 
		global $wpdb;

		if(isset($_GET['post_id']))
			$user_id = $_GET['post_id'];
		else
			$user_id = get_current_user_id();
		//Données du chercheur
		$sql = "SELECT * from {$wpdb->prefix}personnel where Id_pers={$user_id}";
		$req = $wpdb->get_row($sql);

		//Grade du chercheur
		$sql2 = "SELECT * from {$wpdb->prefix}grade where Id_pers = {$user_id} order by Date_fin";
		$req2 = $wpdb->get_results($sql2);

		//HDR du chercheur
		$sql3 = "SELECT * from {$wpdb->prefix}hdr where Id_pers = {$user_id}";
		$req3 = $wpdb->get_row($sql3);

		//Thèse du chercheur
		$sql4 = "SELECT * from {$wpdb->prefix}these where Id_pers = {$user_id}";
		$req4 = $wpdb->get_row($sql4);

		//PEDR du chercheur
		$sql5 = "SELECT * from {$wpdb->prefix}pedr where Id_pers = {$user_id}";
		$req5 = $wpdb->get_row($sql5);


	?>
	<div class="subsubsub">

		<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="<?php echo $user_id ?>"/>
			<p>Nom : <input type="text" name="Nom" value="<?php echo $req->Nom; ?>" required /></p>
 			<p>Prénom : <input type="text" name="Prenom" value="<?php echo $req->Prenom; ?>" required /></p>
 			<p>Date de Naissance : <input type="date" value="<?php echo $req->Date_naissance; ?>" name="Date_naissance" /></p>
 			<p>Email 1 : <input type="email" name="Email1" value="<?php echo $req->Email1; ?>" /></p>
 			<p>Email 2 : <input type="email" name="Email2" value="<?php echo $req->Email2; ?>" /></p>
 			<p>Date entrée : <input type="date" name="Date_entrer" value="<?php echo $req->Date_entrer; ?>" /></p>
 			<p>Date sortie : <input type="date" name="Date_sortie" value="<?php echo $req->Date_sortie; ?>"/></p>
 			<p> Université : <input type="text" name="Université" value="<?php echo $req->Université; ?>" /></p>
 			<p> CNRS : <input type="text" name="CNRS" value="<?php echo $req->CNRS; ?>"/></p>
 			<p> IUF : <input type="text" name="IUF" value="<?php echo $req->IUF; ?>" /></p>

 			<?php echo submit_button('Enregistrer'); ?>
		</form>

		




	<h2> Grades </h2>
	<?php 
	if(!isset($req2)) {

	echo'<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action2" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="';echo $user_id; echo '"/>
 			<p>Date début : <input type="date" name="Date_debut" value="" /></p>
 			<p>Date entrée : <input type="date" name="Date_fin" value=""/></p>' ;

 			echo submit_button('Enregistrer');
		echo '</form>'; }
    elseif(is_null($req2[-1]-> Date_fin)){
    	echo'<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action3" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="';echo $user_id; echo '"/>
		<input type="hidden" name="id2" id="hiddenField" value="';echo $req2[-1]->Id_Grade; echo '"/>
 			<p>Date début : <input type="date" name="Date_debut" value="';echo $req2[-1]->Date_debut; echo '" /></p>
 			<p>Date entrée : <input type="date" name="Date_fin" value=""/></p>' ;

 			echo submit_button('Enregistrer');
		echo '</form>'; }
 
    else
		 foreach($req2 as $grade){
		echo'<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action3" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="';echo $user_id; echo '"/>
		<input type="hidden" name="id2" id="hiddenField" value="';echo $grade->Id_Grade; echo '"/>
 			<p>Date début : <input type="date" name="Date_debut" value="';echo $grade->Date_debut; echo '" /></p>
 			<p>Date entrée : <input type="date" name="Date_fin" value="';echo $grade->Date_fin; echo '"/></p>' ;

 			echo submit_button('Enregistrer');
		echo '</form>'; }



		?>
	<h2> HDR </h2>


	<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action4" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="<?php echo $user_id ?>"/>
		<input type="hidden" name="id_HDR" id="hiddenField" value="<?php echo $req3->Id_HDR ?>"/>
 			<p>Date HDR : <input type="date" name="Date_hdr" value="<?php echo $req3->Date_hdr; ?>" /></p>
 			<p> Libellé : <input type="text" name="Libelle" value="<?php echo $req3->Libelle_hdr; ?>" /></p>

 			<?php echo submit_button('Enregistrer'); ?>
		</form>




	<h2> Thèse </h2>

	<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action5" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="<?php echo $user_id ?>"/>
		<input type="hidden" name="id_HDR" id="hiddenField" value="<?php echo $req4->Id_these  ?>"/>
 			<p>Date Thèse : <input type="date" name="Date_hdr" value="<?php echo $req4->Date_these ; ?>" /></p>
 			<p> Libellé : <input type="text" name="Libelle" value="<?php echo $req4->Libelle_these ; ?>" /></p>

 			<?php echo submit_button('Enregistrer'); ?>
		</form>


	<h2> PEDR </h2>

	<form name="chercheur-profil" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action6" value="vallorem-chercheur-save-action">
		<input type="hidden" name="id" id="hiddenField" value="<?php echo $user_id ?>"/>
		<input type="hidden" name="id_HDR" id="hiddenField" value="<?php echo $req5->Id_pedr  ?>"/>
 			<p>Date_PEDR : <input type="date" name="Date_hdr" value="<?php echo $req5->Date_pedr ; ?>" /></p>
 			<p> Montant : <input type="number" name="Montant" value="<?php echo $req5->Montant ; ?>" /></p>

 			<?php echo submit_button('Enregistrer'); ?>
		</form>

	</div>
	

</div>
