<div class="wrap">
	<h1>
		<?php echo get_admin_page_title() ?>

<?php if ( current_user_can( 'create_users' ) ) { ?>
<a href="user-new.php" class="page-title-action"><?php echo esc_html_x( 'Add New', 'user' ); ?></a>
<?php } elseif ( is_multisite() && current_user_can( 'promote_users' ) ) { ?>
<a href="user-new.php" class="page-title-action"><?php echo esc_html_x( 'Add Existing', 'user' ); ?></a>
<?php } ?>
	</h1>
	<div class="subsubsub">
	</div>
	
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<th scope="col" id='name' class='manage-column column-title column-primary sortable desc'>
					<a href="http://wordpress-mpa/wp-admin/edit.php?orderby=name&#038;order=asc">
						<span>Nom</span>
						<span class="sorting-indicator"></span>
					</a>
				</th>
				<th scope="col" id='name' class='manage-column column-title column-primary sortable desc'>
					<a href="http://wordpress-mpa/wp-admin/edit.php?orderby=name&#038;order=asc">
						<span>Prenom</span>
						<span class="sorting-indicator"></span>
					</a>
				</th>
			</tr>

		</thead>
		<tbody>
			<?php foreach ($chercheurs as $value): ?>
				<tr id="post-4" class="iedit author-self level-0 post-4 type-post status-publish format-standard hentry category-uncategorized">
					
					<td class="title column-title has-row-actions column-primary page-title" data-colname="Nom">
						<strong>
							<a class="row-title"  href="<?php menu_page_url('vallorem-my-chercheur') ?>&amp;post_id=<?php echo $value->Id_pers; ?>" title="Modifier «&nbsp;<?php echo $value->Nom ?>&nbsp;»"><?php echo $value->Nom ?></a>
						</strong>


						<div class="row-actions">
							<span class="edit"><a href="<?php menu_page_url('vallorem-my-chercheur') ?>&amp;post_id=<?php echo $value->Id_pers; ?>" title="Modifier cet élément">Modifier</a> | </span>

						</div>

						<button type="button" class="toggle-row"><span class="screen-reader-text">Afficher plus de détails</span></button>
					</td>

					<td class="type column-type" data-colname="Prenom"><abbr title=""><?php echo $value->Prenom ?></td>		
						
					</tr>
			<?php endforeach ?>
		
			</tbody>

		</div>
