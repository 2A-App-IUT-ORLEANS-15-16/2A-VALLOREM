<div class="wrap">
	<h1><?php echo get_admin_page_title() ?></h1>
	
	<form name="publication" action="admin-post.php" method="post" id="publication-form" autocomplete="off">
		<input type="hidden" name="action" value="vallorem-publication-add-action">
		<div class="input-group" style="display: flex; justify-contenat:space-around; flex-wrap: wrap ">
			<?php foreach ($typesPublication as $key => $value): ?>
					<div style="white-space: nowrap; margin-left: 5px;">
						<input type="radio" id="chkbox-<?php echo $key; ?>" name="type" value= "<?php echo $key ?>"/>
						<label for="chkbox-<?php echo $key; ?>" ><?php echo $value['checkboxLabel'] ?></label>	
					</div>
			<?php endforeach ?>
		</div>

		<table class="form-table">
			<tbody>
				<tr class="input-group">
					<th for="">Auteur</th>
					<td>
						<select name="Id_Pers[]" id="" multiple style="min-width:200px">

							<?php foreach ($auteurs as $value): ?>			
								<option value="<?php echo $value->Id_pers; ?>"><?php echo $value->Nom . ' ' . $value->Prenom ?></option>
							<?php endforeach ?>
						</select>
					</td>
				</tr>
			</tbody>
			
			
			<tbody id="table-content">
				
			</tbody>
		</table>


		<?php echo submit_button(); ?>
	</form>	
	
</div>


<script>
	var publicationType = <?php echo json_encode($typesPublication, true); ?>;
	var publication = <?php echo $publicationJSON; ?>;
	var checkboxes = document.querySelectorAll('#publication-form input[name=type]');

	for(var i = 0; i < checkboxes.length; i++){
		checkboxes[i].addEventListener('change', onTypeChange);
	}

	if(publication != null){
		var form = document.querySelector('#publication-form');
		var inputId = document.createElement('input');
		inputId.type = 'hidden';
		inputId.value = publication['Id_doc'];
		inputId.name = 'Id_doc';
		form.appendChild(inputId);
		if(publication['Categorie'] != null){
			var chkBox = document.querySelector('#chkbox-'+publication['Categorie']);
			if(chkBox != null){
				chkBox.checked = true;
				onTypeChange.bind(chkBox)();
			}
		}

	} 
	function onTypeChange(event){
		var formContent = document.querySelector("#table-content");
		while (formContent.firstChild) formContent.removeChild(formContent.firstChild);

		//console.log(publicationType, this.value);
		var fields = publicationType[this.value]['fields'];
		console.log(fields);
		for(var fieldName in fields){
			var field = fields[fieldName];

			var input = null;

			var inputType = field.type;

			if(inputType.toLowerCase() == 'textarea'){
				input = document.createElement('textarea');
				input.className ="large-text code";
				input.row = 3;

			}else{
				input = document.createElement('input');
				input.type = field.type;
				input.className = "regular-text";
				input.required = field['required'];
			}
			input.name = field.database;	
			if(publication != null && publication[input.name] != null){
				input.value = publication[input.name];
			}



			var label = document.createElement('label');
			label.textContent = field.label;
			var tr = document.createElement('tr');
			tr.className = "input-group";

			var tdLabel = document.createElement('th');
			tdLabel.scope="row";
			tdLabel.appendChild(label);
			
			var tdInput = document.createElement('td');
			tdInput.appendChild(input);
			console.log(field);
			if(field['required']){
				//<p class="description" id="tagline-description">En quelques mots, décrivez la raison d&rsquo;être de ce site.</p>
				var pDesc = document.createElement('p');
				pDesc.className = "description";
				pDesc.id = "tagline-description";

				var span = document.createElement('span');
				span.style.color = "red";
				span.textContent = "Obligatoire";
				pDesc.appendChild(span);
				tdInput.appendChild(pDesc);
			}

			if(field['description'] != null){
				//<p class="description" id="tagline-description">En quelques mots, décrivez la raison d&rsquo;être de ce site.</p>
				var pDesc = document.createElement('p');
				pDesc.className = "description";
				pDesc.id = "tagline-description";

				var span = document.createElement('span');
				span.className = "errror";
				span.textContent = field['description'];
				pDesc.appendChild(span);
				tdInput.appendChild(pDesc);
			}

			tr.appendChild(tdLabel);
			tr.appendChild(tdInput);

			formContent.appendChild(tr);
		}

		
			
	}


</script>