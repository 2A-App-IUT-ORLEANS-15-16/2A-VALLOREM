<div class="wrap">
	<h1>
		<?php echo get_admin_page_title() ?>
		<a href="<?php menu_page_url('vallorem-publication-add') ?>" class="page-title-action">Ajouter</a>
	</h1>
	<div class="subsubsub">
	</div>
	
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<td id='cb' class='manage-column column-cb check-column'>
					<label class="screen-reader-text" for="cb-select-all-1">Tout sélectionner</label>
					<input id="cb-select-all-1" type="checkbox" />
				</td>
				<th scope="col" id='title' class='manage-column column-title column-primary sortable desc'>
					<a href="http://wordpress-mpa/wp-admin/edit.php?orderby=title&#038;order=asc">
						<span>Titre</span>
						<span class="sorting-indicator"></span>
					</a>
				</th>
				<th scope="col" id='author' class='manage-column column-author'>Auteur</th>
				<th scope="col" id='categories' class='manage-column column-type'>type</th>
				<th scope="col" id='validate' class='manage-column column-type'>Validée</th>
				<th scope="col" id='date' class='manage-column column-date sortable asc'>
					<a href="http://wordpress-mpa/wp-admin/edit.php?orderby=date&#038;order=desc">
						<span>Date</span>
						<span class="sorting-indicator"></span>
					</a>
				</th>
			</tr>

		</thead>
		<tbody>
			<?php foreach ($publications as $value): ?>
				<tr id="post-4" class="iedit author-self level-0 post-4 type-post status-publish format-standard hentry category-uncategorized">
					<th scope="row" class="check-column">			<label class="screen-reader-text" for="cb-select-4">Sélectionner <?php echo $value->Titre ?></label>
						<input id="cb-select-4" type="checkbox" name="post[]" value="4">
						<div class="locked-indicator"></div>
					</th>
					<td class="title column-title has-row-actions column-primary page-title" data-colname="Titre">
						<strong>
							<a class="row-title" href="<?php menu_page_url('vallorem-publication-add') ?>&amp;post_id=<?php echo $value->Id_doc; ?>" title="Modifier «&nbsp;<?php echo $value->Titre ?>&nbsp;»"><?php echo $value->Titre ?></a>
						</strong>


						<div class="row-actions">
							<span class="edit"><a href="<?php menu_page_url('vallorem-publication-add') ?>&amp;post_id=<?php echo $value->Id_doc; ?>" title="Modifier cet élément">Modifier</a></span>
							<!-- <span class="trash"><a class="submitdelete" title="Déplacer cet élément dans la Corbeille" href="http://wordpress-mpa/wp-admin/post.php?post=4&amp;action=trash&amp;_wpnonce=a4f5c47ebc">Mettre à la Corbeille</a> | </span> -->
							<?php if ($value->validate == 0 && current_user_can('edit_all_publications')): ?>
								<span class="valider">
									| <a href="admin-post.php?action=vallorem-publication-validate&amp;post_id=<?php echo $value->Id_doc; ?>" title="Afficher «&nbsp;test&nbsp;»" rel="permalink">Valider</a>
								</span>
							<?php endif ?>
						</div>

						<button type="button" class="toggle-row"><span class="screen-reader-text">Afficher plus de détails</span></button>
					</td>
					<!-- Voir si on met l'url d'edition du chercheurs -->
					<td class="author column-author" data-colname="Auteur"><a href=""><?php echo implode(', ',$value->Noms) ?></a></td>
					<td class="type column-type" data-colname="Type"><abbr title=""><?php echo $value->Libelle_type ?></td>		
					<td class="type column-type" data-colname="Type"><abbr title=""><?php echo $value->validate ? 'Validée' : 'Non validée' ?></td>		
					<td class="date column-date" data-colname="Date"><abbr title="05/12/2015 23 h 34 min 46 s">05/12/2015</abbr><br>Publié</td>		
					</tr>
			<?php endforeach ?>
			</tbody>

