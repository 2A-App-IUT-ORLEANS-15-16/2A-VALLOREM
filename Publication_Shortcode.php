<?php 
/**
  * La classe gère les shortCodes qui affiche les documents à afficher pour les différentes pages
  *
  * @author 2A-Apprenti
  * @method function __construct()
  * @method String[] SelectionCategorie(String $categorie)
  * @method String[] SelectionParAnnee(String $doc,String $annee)
  * @method void Documents(Map<String,String> $atts )
  * @method void DocParAnnee(Map<String,String> $atts )
  */
class Publication_Shortcode
{
	/**
	  * Le contructeur ajoute tout les ShortCodes de Valorem
	  * 
	  */
	function __construct()
	{
		include('templates/publications_Shortcode.php'); 
		add_shortcode( 'Documents', [$this,'Documents'] );
		add_shortcode( 'DocParAnnée', [$this,'DocParAnnee']);
	}

	/**
	  * La fonction recherche tout les documents pour une catégorie choisie
	  * @param String $categorie
	  * @return Object[] $publications
	  */
	function SelectionCategorie($categorie){	
		global $wpdb;
		$sql  = $wpdb->prepare("select * from {$wpdb->prefix}documents natural join {$wpdb->prefix}type natural join {$wpdb->prefix}auteur natural join {$wpdb->prefix}personnel where validate=1 and Categorie = %s order by id_pers",$categorie);
		$publications = $wpdb->get_results($sql);
		return $publications ;
	}	

	/**
	  * La fonction recherche tout les documents pour une catégorie choisie et une année choisie
	  * @param String $categorie
	  * @param String $annee
	  * @return Object[] $publications
	  */
	function SelectionParAnnee($categorie,$annee){
		global $wpdb;
		$sql  = $wpdb->prepare("select * from {$wpdb->prefix}documents natural join {$wpdb->prefix}type natural join {$wpdb->prefix}auteur natural join {$wpdb->prefix}personnel where validate=1 and YEAR(Date_doc) = %s and Categorie = %s order by Date_doc",$annee,$categorie);
		$publications = $wpdb->get_results($sql);
		return $publications ;
	}

	/**
	  * La fonction affiche en html tout les documents de la catégorie choisie
	  * par exemple [Documents doc=brevet]
	  * @param String[] $atts
	  */
	function Documents( $atts ) {
		$publications = $this->SelectionCategorie($atts['doc']);
		$affichage = new Affichage();
		$affichage->affichageParNom($publications);
	}

	/**
	  * La fonction affiche en html tout les documents de la catégorie choisie et par année
	  * par exemple [DocParAnnée doc=brevet annee=2015]
	  * @param String[] $atts
	  */
	function DocParAnnee( $atts ) {
		$publications = $this->SelectionParAnnee($atts['doc'],$atts['annee']);
		$affichage = new Affichage();
		$affichage->affichageParDate($publications);
	}
}
