<?php 

class ChercheurShortCode{
	function __construct()
	{
		add_shortcode( 'Chercheurs', [$this,'chercheurs'] );
	}


	function chercheurs(){
		global $wpdb;
		$selectChercheurs = "select * from {$wpdb->prefix}personnel where Date_sortie is null or Date_sortie >= CURDATE()";
		$chercheurs = $wpdb->get_results($selectChercheurs);
		include 'templates/chercheurs-shortcode.php';
	}

}

 ?>