<?php

	/**
	* @author 2A-Apprenti
	* Renvoie l'id du type passé en paramètre. Si le type n'existe pas dans la base de données, celui-ci est créé.
	* @param String $type le type à chercher
	* @param String $libelle le libellé du type (facultatif)
	* @return integer l'id du type
	*/
	function getDocumentTypeIdWithCreate($type, $libelle=null){
		global $wpdb;
		$selectQuery = "select Id_type from {$wpdb->prefix}type where Categorie='{$type}'";
		$id = $wpdb->get_var($selectQuery);
		if($id){
			return $id;
		}
		else {
			if($libelle==null){
				$libelle=$type;
			}
			$wpdb->insert("{$wpdb->prefix}type", ['Libelle_type'=> $libelle, 'Categorie' => $type]);
			return $wpdb->insert_id;
		}

	}

	/**
	* @author 2A-Apprenti
	* Renvoie la liste de toute les publications
	* @param String $where condition de recherche (facultatif)
	* @return String[] la liste des publications
	*/
	function listAllPublications($where=null){
		global $wpdb;
		$query = "select doc.*, pers.*, type.* from {$wpdb->prefix}documents doc join {$wpdb->prefix}auteur aut on aut.Id_doc = doc.Id_doc join {$wpdb->prefix}personnel pers on pers.Id_Pers = aut.Id_Pers left join {$wpdb->prefix}type `type` on `type`.Id_type = doc.id_type";
		if($where != null)
			$query . " where {$where}";

		return $wpdb->get_results($query);
	}

	/**
	* @author 2A-Apprenti
	* Renvoie la liste des publications groupées par Nom de l'auteur
	* @param String $where condition de recherche (facultatif)
	* @return la liste des publications par nom d'auteur
	*/
	function listAllPublicationAvecNomAuteurGrouper($where=null){
		$publicationsRaw = listAllPublications($where);
		$publications = [];
		foreach ($publicationsRaw as $publication) {
			if(!isset($publications[$publication->Id_doc])){
				$publications[$publication->Id_doc] = $publication;
			}
			if(!property_exists($publications[$publication->Id_doc],'Noms'))
				$publications[$publication->Id_doc]->Noms = [];
			$publications[$publication->Id_doc]->Noms[] = $publication->Prenom . ' ' . $publication->Nom;
		}
		return $publications;
	}

	/**
	* @author 2A-Apprenti
	* Renvoie la liste de toutes les personnes
	* @return la liste du personnel
	*/
	function listAllPersonnels(){
		global $wpdb;
		return $wpdb->get_results("select * from {$wpdb->prefix}personnel;");
	}

	/**
	* @author 2A-Apprenti
	* Renvoie la publication à partir de son identifiant
	* @param integer $publicationID l'id de la publication à chercher
	* @return les données de la publication
	*/
	function getPublication($publicationID){
		if($publicationID < 0) return null;

		global $wpdb;
		return $wpdb->get_row("select doc.*, pers.*, type.* from {$wpdb->prefix}documents doc join {$wpdb->prefix}auteur aut on aut.Id_doc = doc.Id_doc join {$wpdb->prefix}personnel pers on pers.Id_Pers = aut.Id_Pers left join {$wpdb->prefix}type `type` on `type`.Id_type = doc.id_type where doc.Id_doc={$publicationID}");

	}
