<?php

/**
* @author 2A-Apprenti
* Classe qui permet d'ajouter une publication
*/
class PublicationAdd{

	function __construct(){
		add_action('admin_menu', [$this, 'adminMenu']);
		add_action('admin_post_vallorem-publication-add-action', [$this, 'action']);

	}
	/**
	* validation du formulaire d'ajout de publication
	*/
	function action(){

		unset($_POST['action']);
		unset($_POST['submit']);
		$type = $_POST['type'];
		$auteursID = [];
		if(isset($_POST['Id_Pers'])){
			$auteursID = $_POST['Id_Pers'];
			unset($_POST['Id_Pers']);
		}
		if(!current_user_can('edit_all_publications') && !in_array(get_current_user_id(), $auteursID)){
			$auteursID[] = get_current_user_id();
		}
		unset($_POST['type']);

		$_POST['Id_Type'] = getDocumentTypeIdWithCreate($type);

		//var_dump($_POST);
		global $wpdb;

		if(isset($_POST['Id_doc'])){
			$wpdb->update("{$wpdb->prefix}documents", $_POST, ['Id_doc' => $_POST['Id_doc']]);
			$wpdb->delete("{$wpdb->prefix}auteur", ['Id_doc' => $_POST['Id_doc']]);
			$docId = $_POST['Id_doc'];
		}
		else{
			$wpdb->insert("{$wpdb->prefix}documents", $_POST);
			$docId = $wpdb->insert_id;

		}
		if(sizeof($auteursID) == 0){
			$wpdb->insert("{$wpdb->prefix}auteur", ['Id_Pers' => get_current_user_id(), 'Id_doc' => $docId]);
		}else{

			foreach ($auteursID as $id) {
				$wpdb->insert("{$wpdb->prefix}auteur", ['Id_Pers' => $id, 'Id_doc' => $docId]);
			}
		}



   		//die();
 		wp_redirect( add_query_arg( array('page' => 'vallorem-publications'), admin_url('admin.php') ));

		exit;
	}

	function adminMenu(){
		add_submenu_page('vallorem-publications', 'Nouvelle publication', null, 'edit_self_publications', 'vallorem-publication-add', array($this, 'render'));
	}

	/**
	* Affiche la formulaire de l'ajout de publication
	*/
	function render(){
		include('configs/types-publication.php');

		$auteurs = listAllPersonnels();
		$publicationId = isset($_GET['post_id']) ? $_GET['post_id'] : -1;
		$publication = getPublication($publicationId);
		$publicationJSON = json_encode($publication, true);

		include('templates/publication-add.php');
	}

}

 ?>
