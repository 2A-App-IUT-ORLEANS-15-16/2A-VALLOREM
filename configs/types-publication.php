<?php 

$typesPublication = [
	'these' => [
		'label' => 'These',
		'checkboxLabel' => 'These',
		'fields' => [
			'titre' => [
				'type' => 'text',
				'label' => 'Titre de la thèse',
				'database' => 'Titre',
                'required' => true
			],
            'resume' => [
                'type' => 'textarea',
                'label' => 'Résumé',
                'database' => 'Resume'
            ],
            'domaine' => [
				'type' => 'text',
				'label' => 'Domaine',
				'database' => 'Domaine',
                'required' => true
			],
			'universite' => [
				'type' => 'text',
				'label' => 'Université',
				'database' => 'Universite',
                'required' => true
			],
            'lieu' => [
                'type' => 'text',
                'label' => 'Lieu',
                'database' => 'Lieu'
            ],
            'Date_doc' => [
                'type' => 'date',
                'label' => 'Date',
                'database' => 'Date_doc',
                'required' => true
            ],
            'nb_page' => [
                'type' => 'number',
                'label' => 'Nombre de page',
                'database' => 'Nb_page'
            ],
            'langue' => [
                'type' => 'text',
                'label' => 'Langue',
                'database' => 'Langue'
            ],
            'titre_abreger' => [
                'type' => 'text',
                'label' => 'Titre abrégé',
                'database' => 'Titre_abreger'
            ],
            'url' => [
                'type' => 'text',
                'label' => 'URL',
                'database' => 'URL'
            ],
            'jury' => [
                'type' => 'text',
                'label' => 'Jury',
                'database' => 'Jury'
            ],
            'archive' => [
                'type' => 'text',
                'label' => 'Archive',
                'database' => 'Archive'
            ],
            'location_archive' => [
                'type' => 'text',
                'label' => 'Localisaion dans l\'archive',
                'database' => 'Location_archive'
            ],
            'catalogue' => [
                'type' => 'text',
                'label' => 'Catalogue de bibliothèque',
                'database' => 'Catalogue'
            ],
            'cote' => [
                'type' => 'text',
                'label' => 'Côte',
                'database' => 'Cote'
            ]
		]
	],
	'article' => [
		'label' => 'Article de revue',
		'checkboxLabel' => 'Article de revue',
		'fields' => [
			'titre_article' => [
				'type' => 'text',
				'label' => 'Titre de l\'article',
				'database' => 'Titre', 
				'required' => true
			],
			'titre_periodique' => [
				'type' => 'text',
				'label' => 'Titre du périodique',
				'database' => 'Titre_article', 
                'required' => true
			],
            'Date_doc' => [
                'type' => 'date',
                'label' => 'Année de publication',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'volume' => [
                'type' => 'text',
                'label' => 'Volume',
                'database' => 'Volume'
            ],
            'numero' => [
                'type' => 'text',
                'label' => 'Numéro',
                'database' => 'Numero'
            ],
            'pagination' => [
                'type' => 'text',
                'label'=> 'Pagination',
                'database' => 'Pagination'
            ],
            'issn' => [
                'type' => 'text',
                'label' => 'ISSN (8 caractères)',
                'database' => 'ISBN'
            ]
	    ]

	],
    'communication' => [
        'label' => 'Communication',
		'checkboxLabel' => 'Communication',
		'fields' => [
			'titre' => [
				'type' => 'text',
				'label' => 'Titre de la communication',
				'database' => 'Titre', 
				'required' => true
			],
            'nom_maitre' => [
                'type' => 'text',
                'label' => 'Nom du maître de conférence',
                'database' => 'Nom_maitre'
            ],
            'prenom_maitre' => [
                'type' => 'text',
                'label' => 'Prénom du maître de conférence',
                'database' => 'Prenom_maitre'
            ],
            'titre_conf' => [
                'type' => 'text',
                'label' => 'Titre de la conférence',
                'database' => 'Titre_conference', 
                'required' => true
            ],
            'date_conf' => [
                'type' => 'date',
                'label' => 'Date de la conférence',
                'database' => 'Date_conf', 
                'required' => true
            ],
            'lieu_conf' => [
                'type' => 'text',
                'label' => 'Lieu',
                'database' => 'Lieu_conf', 
                'required' => true
            ],
            'lieu_edition' => [
                'type' => 'text',
                'label' => 'Lieu d\'édition',
                'database' => 'Lieu_edition'
            ],
            'editeur' => [
                'type' => 'text',
                'label' => 'Editeur commercial',
                'database' => 'Editeur_com'
            ],
            'annee' => [
                'type' => 'date',
                'label' => 'Année de publication',
                'database' => 'Date_doc'
            ],
            'nb_pages' => [
                'type' => 'number',
                'label' => 'Nombre de pages',
                'database' => 'Nb_page'
            ],
            'titre_collection' => [
                'type' => 'text',
                'label' => 'Titre de la collection',
                'database' => 'Titre_Collection'
            ],
            'numero' => [
                'type' => 'number',
                'label' => 'n° de la collection',
                'database' => 'Numero'
            ],
            'isbn' => [
                'type' => 'text',
                'label' => 'ISBN (10 caractères)',
                'database' => 'ISBN'
            ]
        ]
    ],
    'ouvrage' => [
        'label' => 'Ouvrage (chapitre)',
		'checkboxLabel' => 'Ouvrage',
		'fields' => [
			'titre' => [
				'type' => 'text',
				'label' => 'Titre de l\'ouvrage',
				'database' => 'Titre', 
				'required' => true
			],
            'tomaison' => [
                'type' => 'text',
                'label' => 'Tomaison',
                'database' => 'Volume'
            ],
            'edition' => [
                'type' => 'text',
                'label' => 'Edition',
                'database' => 'Edition', 
                'required' => true
            ],
            'lieu_edition' => [
                'type' => 'text',
                'label' => 'Lieu d\'édition',
                'database' => 'Lieu_edition'
            ],
            'editeur' => [
                'type' => 'text',
                'label' => 'Editeur commercial',
                'database' => 'Editeur_com'
            ],
            'Date_doc' => [
                'type' => 'date',
                'label' => 'Année de publication',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'nbPage' => [
                'type' => 'number',
                'label' => 'Nombre de pages',
                'database' => 'Nb_page'
            ],
            'titre_collection' => [
                'type' => 'text',
                'label' => 'Titre de la collection',
                'database' => 'Titre_Collection'
            ],
            'numero' => [
                'type' => 'number',
                'label' => 'n° de la collection',
                'database' => 'Numero'
            ],
            'format' => [
                'type' => 'text',
                'label' => 'Format',
                'database' => 'Format'
            ],
            'url' => [
                'type' => 'text',
                'label' => 'URL',
                'database' => 'URL'
            ],
            'date_consultation' => [
                'type' => 'date',
                'label' => 'Date de consultation',
                'database' => 'Date_consultation'
            ],
            'isbn' => [
                'type' => 'text',
                'label' => 'ISBN (10 caractères)',
                'database' => 'ISBN'
            ]
        ]
    ],
    'brevet' => [
        'label' => 'Brevet',
        'checkboxLabel' => 'Brevet',
        'fields' => [
            'titre' => [
                'type' => 'text',
                'label' => 'Titre du brevet',
                'database' => 'Titre', 
                'required' => true
            ],
            'pays' => [
                'type' => 'text',
                'label' => 'Pays',
                'database' => 'Pays', 
                'required' => true
            ],
            'type' => [
                'type' => 'text',
                'label' => 'Type de document de brevet',
                'database' => 'type_brevet'
            ],
            'numero' => [
                'type' => 'number',
                'label' => 'n° de la collection',
                'database' => 'Numero'
            ],
            'Date_doc' => [
                'type' => 'date',
                'label' => 'Date de publication',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'url' => [
                'type' => 'text',
                'label' => 'URL',
                'database' => 'URL'
            ]
        ]
    ],
    'hdr' => [
        'label' => 'HDR',
        'checkboxLabel' => 'HDR',
        'fields' => [
            'titre' => [
                'type' => 'text',
                'label' => 'Titre de l\'HDR',
                'database' => 'Titre', 
                'required' => true
            ],
            'resume' => [
                'type' => 'textarea',
                'label' => 'Résumé',
                'database' => 'Resume'
            ],
            'domaine' => [
                'type' => 'text',
                'label' => 'Domaine',
                'database' => 'Domaine', 
                'required' => true
            ],
            'universite' => [
                'type' => 'text',
                'label' => 'Universite',
                'database' => 'Universite', 
                'required' => true
            ],
            'lieu' => [
                'type' => 'text',
                'label' => 'Lieu',
                'database' => 'Lieu'
            ],
            'Date_doc' => [
                'type' => 'date',
                'label' => 'Date',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'nb_page' => [
                'type' => 'number',
                'label' => 'Nombre de page',
                'database' => 'Nb_page'
            ],
            'langue' => [
                'type' => 'text',
                'label' => 'Langue',
                'database' => 'Langue'
            ],
            'titre_abreger' => [
                'type' => 'text',
                'label' => 'Titre abrégé',
                'database' => 'Titre_abreger'
            ],
            'url' => [
                'type' => 'text',
                'label' => 'URL',
                'database' => 'URL'
            ],
            'jury' => [
                'type' => 'text',
                'label' => 'Jury',
                'database' => 'Jury'
            ],
            'archive' => [
                'type' => 'text',
                'label' => 'Archive',
                'database' => 'Archive'
            ],
            'location_archive' => [
                'type' => 'text',
                'label' => 'Localisation dans l\'archive',
                'database' => 'Location_archive'
            ],
            'catalogue' => [
                'type' => 'text',
                'label' => 'Catalogue de bibliothèque',
                'database' => 'Catalogue'
            ],
            'cote' => [
                'type' => 'text',
                'label' => 'Côte',
                'database' => 'Cote'
            ]
        ]
    ],
    'logiciel' => [
        'label' => 'Logiciel',
        'checkboxLabel' => 'Logiciel',
        'fields' => [
            'nom' => [
                'type' => 'text',
                'label' => 'Nom du logiciel',
                'database' => 'Titre', 
                'required' => true
            ],
            'support' => [
                'type' => 'text',
                'label' => 'Support du logiciel',
                'database' => 'Support_logiciel'
            ],
            'version' => [
                'type' => 'text',
                'label' => 'Numéro version',
                'database' => 'Version', 
                'required' => true
            ],
            'ville' => [
                'type' => 'text',
                'label' => 'Ville',
                'database' => 'Ville'
            ],
            'lieu' => [
                'type' => 'text',
                'label' => 'Lieu (université, laboratoire...',
                'database' => 'Lieu_Creation'
            ],
            'Date_doc' => [
                'type' => 'text',
                'label' => 'Année de parution',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'os' => [
                'type' => 'text',
                'label' => 'Sysème d\'exploitation',
                'database' => 'OS',
                'description' => '(Windows, Linux, MacOS) Séparer par un \';\' s\'il existe plusieurs systèmes.'
            ],
            'url' => [
                'type' => 'text',
                'label' => 'URL',
                'database' => 'URL'
            ]
        ]
    ],
    'database' => [
        'label' => 'Base de données',
        'checkboxLabel' => 'Base de données',
        'fields' => [
            'titre' => [
                'type' => 'text',
                'label' => 'Titre de la base',
                'database' => 'Titre', 
                'required' => true
            ],
            'Date_doc' => [
                'type' => 'text',
                'label' => 'Année de création',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'commanditaire' => [
                'type' => 'text',
                'label' => 'Commanditaire du projet',
                'database' => 'Commanditaire'
            ],
            'ville' => [
                'type' => 'text',
                'label' => 'Ville',
                'database' => 'Ville'
            ],
            'lieu' => [
                'type' => 'text',
                'label' => 'Lieu (université, laboratoire...)',
                'database' => 'Lieu_Creation'
            ],
            'url' => [
                'type' => 'text',
                'label' => 'URL',
                'database' => 'URL'
            ]
        ]
    ],
    'participation' => [
        'label' => 'Participation',
        'checkboxLabel' => 'Participation',
        'fields' => [
            'titre' => [
                'type' => 'text',
                'label' => 'Libellé projet',
                'database' => 'Titre', 
                'required' => true
            ],
             'Date_doc' => [
                'type' => 'text',
                'label' => 'Date',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'lieu' => [
                'type' => 'text',
                'label' => 'Lieu de la signature',
                'database' => 'Lieu_Creation', 
                'required' => true
            ],
            'resume' => [
                'type' => 'textarea',
                'label' => 'Résumé',
                'database' => 'Resume', 
                'required' => true
            ],
            'commanditaire' => [
                'type' => 'text',
                'label' => 'Commanditaire',
                'database' => 'Commanditaire', 
                'required' => true
            ]
        ]
    ],
    'contrat_industriel' => [
        'label' => 'Contrat industriel',
        'checkboxLabel' => 'Contrat industriel',
        'fields' => [
            'resume' => [
                'type' => 'textarea',
                'label' => 'Résumé',
                'database' => 'Resume'
            ],
            'titre' => [
                'type' => 'text',
                'label' => 'Libellé du contrat',
                'database' => 'Titre', 
                'required' => true
            ],
            'partenaires' => [
                'type' => 'text',
                'label' => 'Partenaires',
                'database' => 'Parternaire',
                'description' => "Séparer les partenaires par un ';' s'il existe plusieurs partenaires."
            ],
            'Date_doc' => [
                'type' => 'Date',
                'label' => 'Date de début',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'date_fin' => [
                'type' => 'Date',
                'label' => 'Date de fin',
                'database' => 'Date_fin'
            ],
            'nom_referent' => [
                'type' => 'text',
                'label' => 'Nom du référent',
                'database' => 'Nom_maitre'
            ],
            'prenom_referent' => [
                'type' => 'text',
                'label' => 'Prénom du référent',
                'database' => 'Prenom_maitre'
            ]
        ]
    ],
        'autre' => [
        'label' => 'Autre',
        'checkboxLabel' => 'Autre',
        'fields' => [
            'titre_article' => [
                'type' => 'text',
                'label' => 'Titre',
                'database' => 'Titre', 
                'required' => true
            ],
            'autre_type' => [
                'type' => 'text',
                'label' => 'Type',
                'database' => 'Autre_type',
                'required' => true
            ],
            'titre_periodique' => [
                'type' => 'text',
                'label' => 'Titre du périodique',
                'database' => 'Titre_article', 
                'required' => true
            ],
            'Date_doc' => [
                'type' => 'date',
                'label' => 'Année de publication',
                'database' => 'Date_doc', 
                'required' => true
            ],
            'volume' => [
                'type' => 'text',
                'label' => 'Volume',
                'database' => 'Volume'
            ],
            'numero' => [
                'type' => 'text',
                'label' => 'Numéro',
                'database' => 'Numero'
            ],
            'pagination' => [
                'type' => 'text',
                'label'=> 'Pagination',
                'database' => 'Pagination'
            ],
            'issn' => [
                'type' => 'text',
                'label' => 'ISSN (8 caractères)',
                'database' => 'ISBN'
            ]
        ]
    ]
]

?>