<?php 

//require_once('ActionTest.php');

/**
  * La classe gère le menu d'affichage des publications
  *
  * @author 2A-Apprenti
  */
class PublicationsSubMenu
{
	
	function __construct()
	{
		//var_dump(add_submenu_page);
		add_action("admin_menu", [$this, 'adminMenu']); //Appel du adminMenu()
		add_action('admin_post_vallorem-publication-validate', [$this, 'validate']);
		//new ActionTest();
	}

	function adminMenu(){
		add_submenu_page(VALLOREM_MENU_SLUG, 'Publications', 'Publications', 'edit_self_publications', 'vallorem-publications', array($this, 'render')); //Creation d'un sous-menu
	}

	function render(){	
		global $wpdb;
		$userId = get_current_user_id();
		$publicationsSQLWhere = "aut.Id_pers={$userId}";
		if(current_user_can('edit_all_publications')){
			$publicationsSQLWhere = null;
		}

		$publications = listAllPublicationAvecNomAuteurGrouper($publicationsSQLWhere);

		include('templates/publications-submenu.php'); 
	}	

	function validate(){
		global $wpdb;

		if(isset($_GET['post_id'])){
			$wpdb->update("{$wpdb->prefix}documents", ['validate' => true], ['Id_Doc' => $_GET['post_id']]);
		}
		wp_redirect( add_query_arg( array('page' => 'vallorem-publications'), admin_url('admin.php') ));
		exit;
	}
}


 ?>