<?php
/**
 * @package Vallorem Plugin
 * @version 1.0
 */
/*
Plugin Name: Vallorem Plugin
Description: Plugin pour le laboratoire Vallorem
Version: 1.0
Text Domain: ValloremPlugin
*/

require_once('constants.php');
require_once('functions.php');
require_once('PublicationsSubMenu.php');
require_once('PublicationAdd.php');
require_once('ChercheurSubMenu.php');
require_once('Publication_Shortcode.php');
require_once('ChercheursSubMenu.php');
require_once('ChercheurSave.php');
require_once('ChercheurShortCode.php');

/**
* Plugin Vallorem
*/
class ValloremPlugin
{
	
	function __construct()
	{
		//$this->init();	
		add_action('init', [$this, 'init']);
	}

	/**
	* Ajout des subMenu, des rôles, et des possibilités des rôles.
	*/
	function init(){
		global $wpdb;
		
    	
		add_action('admin_menu', array($this, 'registerAdminMenu'));
		//Appel des subMenu
		new PublicationsSubMenu();
		new PublicationAdd();
		new ChercheurSubMenu();
		new Publication_Shortcode();
		new ChercheursSubMenu();
		new ChercheurSave();
		new ChercheurShortCode();

		//Roles
		add_role( 'simple_chercheur', 'Chercheurs', array(
            'read' => true,
            'edit_posts' => false,
            'upload_files' => false,
            'vallorem' => true,
            'edit_self_publications' => true,
            'edit_self_profil' => true,
            'edit_all_publications' => false,
            'edit_all_profil' => false
            ) );

		add_role( 'admin_vallorem', 'Gestion Vallorem', array(
            'read' => true,
            'edit_posts' => false,
            'upload_files' => false,
            'vallorem' => true,
            'edit_self_publications' => true,
            'edit_self_profil' => true,
            'edit_all_publications' => true,
            'edit_all_profil' => true
            ) );

		
		//Ajout de tous les capabilites a l'admin
		$adminRole = get_role( 'administrator' );

	    // This only works, because it accesses the class instance.
	    // would allow the author to edit others' posts for current theme only
	    $adminRole->add_cap( 'vallorem' ); 
	    $adminRole->add_cap( 'edit_all_publications' ); 
	    $adminRole->add_cap( 'edit_self_profil' ); 
	    $adminRole->add_cap( 'edit_self_publications' ); 
	    $adminRole->add_cap( 'edit_all_profil' ); 

	    $gestionRole = get_role( 'admin_vallorem' );

	    // This only works, because it accesses the class instance.
	    // would allow the author to edit others' posts for current theme only
	    $gestionRole->add_cap( 'list_users' ); 
	    $gestionRole->add_cap( 'edit_users' ); 
	    $gestionRole->add_cap( 'create_users' ); 
	    $gestionRole->add_cap( 'edit_posts' ); 
	    $gestionRole->add_cap( 'edit_pages' ); 
	    $gestionRole->add_cap( 'edit_others_posts' ); 
	    $gestionRole->add_cap( 'edit_others_pages' ); 
	    $gestionRole->add_cap( 'publish_posts' ); 
	    $gestionRole->add_cap( 'publish_pages' ); 
	    $gestionRole->add_cap( 'edit_published_posts' ); 
	    $gestionRole->add_cap( 'edit_published_pages' ); 
	     

	    add_action( 'login_enqueue_scripts', [$this, 'login_logo'] );

	    $this->creerFichePersonnel(get_current_user_id());
	    add_action( 'user_register', [$this, 'creerFichePersonnel'], 10, 1 );

	}

	/**
	* Ajout du logo de connexion
	*/
	function login_logo(){
		$stylesheetDir = get_stylesheet_directory_uri();
		
	    echo "<style type=\"text/css\">
	        .login h1 a {
	            background-image: url({$stylesheetDir}/images/logo-vallorem.jpg);
	            padding-bottom: 30px;
	        }
	    </style>";
		}

	/**
	* Force la création à la connexion d'une personne
	*/
	function creerFichePersonnel($user_id){
		if($user_id == 0) return;
		global $wpdb;
		//$user_id = ;
		$testUserQuery = "select count(*) from {$wpdb->prefix}personnel where Id_Pers={$user_id}";
		if($wpdb->get_var($testUserQuery) == 0){
			$current_user = get_userdata($user_id);
			$wpdb->insert("{$wpdb->prefix}personnel", ['Id_Pers' => $user_id, 'Nom' => $current_user->last_name, 'Prenom' => $current_user->first_name ]);

		}
	}

	/**
	* Affichage du menu pour vallorem
	*/
	function registerAdminMenu(){
		add_menu_page (
			        'Vallorem',
			        'Vallorem',
			        'vallorem',
			    	VALLOREM_MENU_SLUG,
			        [$this, 'render']
			    );
	}

	function render(){
		echo '<h1>'.get_admin_page_title().'</h1>';
	}
	
	/**
	* Créer la BDD 
	*/
	public static function install(){
    	global $wpdb;

    	$filename = dirname(__FILE__) . "/vallorem.sql";
		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		fclose($handle);
		$tables = explode(';', $contents);
		//var_dump($tables);
		foreach($tables as $table){
			if($table == '') continue;
    		$wpdb->query($table . ';');
		}
		include('configs/types-publication.php');
		
		//creation automatique des types
		foreach($typesPublication as $key => $value){
			getDocumentTypeIdWithCreate($key, $value['label']);
			// $sqlExists = "select count(*) from {$wpdb->prefix}type where Categorie='{$key}'";
			// if(!$wpdb->get_var($sqlExists)){
			// 	$wpdb->insert("{$wpdb->prefix}type", ['Libelle_type'=> $value['label'], 'Categorie' => $key]);
			// }
		}
		//die();
    	//$wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}vallorem_these (id INT AUTO_INCREMENT PRIMARY KEY, libThese VARCHAR(255) NOT NULL);");
    	ValloremPlugin::testInsertColumns("{$wpdb->prefix}documents", 'validate', 'TINYINT(1) DEFAULT 0');
    	ValloremPlugin::testInsertColumns("{$wpdb->prefix}documents", 'Date_fin', 'DATE');
    	ValloremPlugin::testInsertColumns("{$wpdb->prefix}documents", 'Parternaire', 'VARCHAR(500)');
	}

	/**
	* Test si la colonne existe ou pas
	*/
	public static function testInsertColumns($table, $column, $type){
		global $wpdb; 
		$queryTest = "SHOW COLUMNS from $table like '$column'";
		try{
			$rows = $wpdb->get_results($queryTest);
			if(sizeof($rows) == 0){
				$queryAlter = "alter table $table add $column $type";
				$wpdb->query($queryAlter);
			}
		}catch(Exception $e){
		}
	}
	

}
register_activation_hook(__FILE__, array('ValloremPlugin', 'install'));
new ValloremPlugin();

