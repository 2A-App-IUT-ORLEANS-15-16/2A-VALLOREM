<?php 

//require_once('ActionTest.php');

/**
  * La classe affiche les chercheurs
  *
  * @author 2A-Apprenti
  */
class ChercheursSubMenu
{
	
	function __construct()
	{
		//var_dump(add_submenu_page);
		add_action("admin_menu", [$this, 'adminMenu']); //Appel du adminMenu()
		//new ActionTest();
	}

	function adminMenu(){
		add_submenu_page(VALLOREM_MENU_SLUG, 'Chercheurs', 'Chercheurs', 'edit_all_profil', 'vallorem-chercheurs', array($this, 'render')); //Creation d'un sous-menu
	}

	function render(){	
		global $wpdb;
		$userId = get_current_user_id();

		

		$chercheurs = listAllPersonnels();
		
		include('templates/chercheurs-submenu.php'); 
	}	
}


 ?>